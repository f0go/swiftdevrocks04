//
//  CollectionViewController.swift
//  SwiftDevRockS04
//
//  Created by f0go on 3/1/17.
//  Copyright © 2017 f0go. All rights reserved.
//

import Foundation
import UIKit

class CollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var pageIndicator: UIPageControl!
    
    @IBOutlet weak var mainImage: UIImageView!
    override func viewDidLoad() {
        pageIndicator.numberOfPages = arr.count
    }
    
    let arr = [UIImage(named: "rayo"),UIImage(named: "rayo"),UIImage(named: "rayo"),UIImage(named: "rayo"),UIImage(named: "rayo"),UIImage(named: "header"),UIImage(named: "header"),UIImage(named: "header"),UIImage(named: "header"),UIImage(named: "header")]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCollectionCell", for: indexPath) as! MyCollectionCell
        cell.cellImage.image = arr[indexPath.row]
        cell.label.text = "\(indexPath.row)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        mainImage.image = arr[indexPath.row]
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset.x)
        pageIndicator.currentPage = Int(scrollView.contentOffset.x) / Int(view.bounds.width)
    }

    
}


class MyCollectionCell: UICollectionViewCell {
    @IBOutlet weak var cellImage: UIImageView!
    
    @IBOutlet weak var label: UILabel!
}
