//
//  DetailViewController.swift
//  SwiftDevRockS04
//
//  Created by f0go on 3/1/17.
//  Copyright © 2017 f0go. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {
    var text: String!
    
    @IBOutlet weak var label: UILabel!
    
    
    override func viewDidLoad() {
        label.text = text
    }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
