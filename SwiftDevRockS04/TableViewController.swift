//
//  TableViewController.swift
//  SwiftDevRockS04
//
//  Created by f0go on 3/1/17.
//  Copyright © 2017 f0go. All rights reserved.
//

import Foundation
import UIKit

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table: UITableView!
    
    let myArray = ["Uno", "Dos", "Tres", "Cuatro", "Cinco"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        if indexPath.row == 0 {
            let aux = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! HeaderCell
            cell = aux
        } else {
            let aux = tableView.dequeueReusableCell(withIdentifier: "MyTableCell", for: indexPath) as! MyTableCell
            aux.label.text = myArray[indexPath.row - 1]
            aux.cellImage.image = nil
            aux.cellImage.image = UIImage(named: "rayo")
            aux.button.tag = indexPath.row - 1
            cell = aux
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            let detail = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            detail.text = myArray[indexPath.row - 1]
            navigationController?.pushViewController(detail, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        }
        return 44
    }
    
    @IBAction func cellButton(_ sender: UIButton) {
        print(sender.tag)
    }
    
    
}





class MyTableCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var button: UIButton!
}

class HeaderCell: UITableViewCell {
    
}
