//
//  ViewController.swift
//  SwiftDevRockS04
//
//  Created by f0go on 2/8/17.
//  Copyright © 2017 f0go. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var myswitch: UISwitch!
    @IBOutlet weak var boton: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var segment: UISegmentedControl!

    var variable = ""
    
    @IBAction func actiondos(_ sender: UIButton) {
        print("TOUCH DOWN BOTON")
        sender.setTitleColor(UIColor.red, for: .normal)
    }
    
    @IBAction func action(_ sender: UIButton) {
        print("SUELTO BOTON")
        sender.setTitleColor(UIColor.blue, for: .normal)
        label.text = "Apreté el botón 😝"
    }
    
    

    override func viewDidLoad() {
        
        imageview.image = UIImage(named: "rayo")
        
        print(myswitch.isOn)
        
        if myswitch.isOn {
            spinner.startAnimating()
        }
        
        label.text = "TEXTOTEXTOTEEXTOTEXTOTEXTO"
        label.textColor = UIColor.red
        boton.setTitleColor(UIColor.green, for: .normal)
        
        segment.selectedSegmentIndex = 1
        view.backgroundColor = changeBackgroundColor(index: segment.selectedSegmentIndex)
        
        var variable = "Hola"
        let constante = "Chau"
        var cualquiera:Float!
        
        if let lala = cualquiera {
            print(lala)
        }
        
        
        
        print(variable)
        print(constante)
        
        if variable == "Hola" && constante != "lala" {
            print("Ok")
        }
        
        let miArray:[Int] = [0,1,2,3,4,5,6,7,8,9]
        let arrayFeo: Array<String> = []
        
        let dict: [String:String] = ["hola":"Hello"]
        let dictFeo: Dictionary<String,String> = [:]
        
        print(dict["hola"]!)
        
        for data in miArray {
            print(data)
        }
        
        for i:Int in 0..<miArray.count {
            print(miArray[i])
        }
        
        progress.setProgress(1, animated: true)
        
    }

    
    @IBAction func segment(_ sender: UISegmentedControl) {
        view.backgroundColor = changeBackgroundColor(index: sender.selectedSegmentIndex)
    }
    

    func changeBackgroundColor(index: Int) -> UIColor {
        switch index {
        case 0:
            return UIColor.green
        default:
            return UIColor.yellow
        }
    }
    

    
    @IBAction func textfield(_ sender: UITextField) {
        print(sender.text)
        if sender.text?.characters.count == 4 {
            view.backgroundColor = UIColor.purple
        }else {
            view.backgroundColor = changeBackgroundColor(index: segment.selectedSegmentIndex)
        }
    }
    
    @IBAction func slider(_ sender: UISlider) {
        print(Int(sender.value))
        print("\(Int(sender.value)) eirjsnefd")
        progress.setProgress(sender.value/100, animated: false)
    }
    
    @IBAction func myswitch(_ sender: UISwitch) {
        print(sender.isOn)
        if sender.isOn {
            spinner.startAnimating()
        }else {
            spinner.stopAnimating()
        }
    }
 
    
}
